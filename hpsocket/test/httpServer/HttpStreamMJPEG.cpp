// ConsoleApplication1.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include <iostream>
#include <thread>
#include "MJPGSender.h"

int main()
{
    HttpServerWrap listener(HttpServer_Creator{});
    MJPGSender s_mjpgSender(listener);
    std::cout << "Hello World!\n";
    s_mjpgSender.init("0.0.0.0", 8080);

    s_mjpgSender.createPubChannel(TEST_CHANNEL);

    if (!s_mjpgSender.open()) {
        std::cout << "open httpserver failed." << std::endl;
        return -1;
    }

    while (1) {
        s_mjpgSender.pubTestFrame();
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    return 0;
}
