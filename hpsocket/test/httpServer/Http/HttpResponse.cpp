#include "HttpResponse.h"

#include "HttpContext.h"

HttpResponse::HttpResponse(HttpContext& context) :
    m_cxt(context){
	setStatusCode(EnHttpStatusCode::HSC_OK);
}

HttpResponse::~HttpResponse() {

}

void HttpResponse::setHeader(const std::string &key, const std::string &value) {
	m_mapHeader[key] = value;
}

void HttpResponse::writeData(const char* data, int len)
{
	std::lock_guard<std::mutex> locker(m_mxtBuf);
	m_buffer.write(data, len);
}

void HttpResponse::setStatusCode(EnHttpStatusCode code)
{
	m_statusCode = code;
}

