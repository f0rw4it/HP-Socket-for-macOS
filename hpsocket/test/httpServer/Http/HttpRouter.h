#pragma once
#include <memory>
#include <string>
#include <functional>
#include <vector>
#include <mutex>

struct rax;
class HttpRequest;
class HttpResponse;
class Router
{
public:
	using callback_type = std::function<void(HttpRequest* req, HttpResponse* resp)>;

	Router();
	~Router();

	bool Add(const std::string& uri, callback_type cb);
	bool Lookup(const std::string& uri, callback_type& cb);
private:
	std::shared_ptr<rax> m_rtRouter;
	std::vector<callback_type> m_vecCallback; //记录回调
};

