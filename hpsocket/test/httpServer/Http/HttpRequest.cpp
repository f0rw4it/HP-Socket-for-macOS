#include "HttpRequest.h"

#include "HttpContext.h"

HttpRequest::HttpRequest(HttpContext& context) :
        m_cxt(context){

}

void HttpRequest::writeData(const char *data, int len) {
    std::lock_guard<std::mutex> locker(m_mxtBuf);
    m_buffer.write(data, len);
}

std::string HttpRequest::getBody() {
    return m_buffer.str();
}

CONNID HttpRequest::Connid() const {
    return m_cxt.m_connId;
}
