#include "HttpHelper.h"
#include <unordered_map>
#include <string>

std::unordered_map<std::string, HttpMethod> method = {
	{HTTP_METHOD_POST_TXT, HttpMethod::HTTP_METHOD_POST},
	{HTTP_METHOD_PUT_TXT, HttpMethod::HTTP_METHOD_PUT},
	{HTTP_METHOD_PATCH_TXT, HttpMethod::HTTP_METHOD_PATCH},
	{HTTP_METHOD_GET_TXT, HttpMethod::HTTP_METHOD_GET},
	{HTTP_METHOD_DELETE_TXT, HttpMethod::HTTP_METHOD_DELETE},
	{HTTP_METHOD_TRACE_TXT, HttpMethod::HTTP_METHOD_TRACE},
	{HTTP_METHOD_OPTIONS_TXT, HttpMethod::HTTP_METHOD_OPTIONS},
	{HTTP_METHOD_CONNECT_TXT, HttpMethod::HTTP_METHOD_CONNECT},
};

std::string toString(HttpMethod m) {
	switch (m)
	{
	case HttpMethod::HTTP_METHOD_GET:
		return HTTP_METHOD_GET_TXT;
	case HttpMethod::HTTP_METHOD_POST:
		return HTTP_METHOD_POST_TXT;
	case HttpMethod::HTTP_METHOD_PUT:
		return HTTP_METHOD_PUT_TXT;
	case HttpMethod::HTTP_METHOD_DELETE:
		return HTTP_METHOD_DELETE_TXT;
	case HttpMethod::HTTP_METHOD_PATCH:
		return HTTP_METHOD_PATCH_TXT;
	case HttpMethod::HTTP_METHOD_HEAD:
		return HTTP_METHOD_HEAD_TXT;
	case HttpMethod::HTTP_METHOD_TRACE:
		return HTTP_METHOD_TRACE_TXT;
	case HttpMethod::HTTP_METHOD_OPTIONS:
		return HTTP_METHOD_OPTIONS_TXT;
	case HttpMethod::HTTP_METHOD_CONNECT:
		return HTTP_METHOD_CONNECT_TXT;
	}
	return HTTP_METHOD_UNKNOW_TXT;
}

HttpMethod toMethod(const std::string& m) {
    auto res = toUpper(m);
	auto iter = method.find(res);
	if (iter != method.end())
		return iter->second;

	return HttpMethod::HTTP_METHOD_UNKNOW;
}

std::string toUpper(const std::string &val) {
    std::string res;
    res.resize(val.size());
    for (int i = 0; i < val.length(); ++i) {
        res[i] = (char)toupper(val[i]);
    }
    return res;
}
